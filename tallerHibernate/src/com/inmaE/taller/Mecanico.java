package com.inmaE.taller;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Mecanico {
    private int id;
    private String nombremecanico;
    private String apellido;
    private Date fechaNacimiento;
    private int aniosexperiencia;
    private List<Coche> coches;
    private Taller taller;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombremecanico")
    public String getNombremecanico() {
        return nombremecanico;
    }

    public void setNombremecanico(String nombremecanico) {
        this.nombremecanico = nombremecanico;
    }

    @Basic
    @Column(name = "apellido")
    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @Basic
    @Column(name = "fecha_nacimiento")
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Basic
    @Column(name = "aniosexperiencia")
    public int getAniosexperiencia() {
        return aniosexperiencia;
    }

    public void setAniosexperiencia(int aniosexperiencia) {
        this.aniosexperiencia = aniosexperiencia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mecanico mecanico = (Mecanico) o;
        return id == mecanico.id &&
                aniosexperiencia == mecanico.aniosexperiencia &&
                Objects.equals(nombremecanico, mecanico.nombremecanico) &&
                Objects.equals(apellido, mecanico.apellido) &&
                Objects.equals(fechaNacimiento, mecanico.fechaNacimiento);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombremecanico, apellido, fechaNacimiento, aniosexperiencia);
    }

    @ManyToMany(mappedBy = "mecanicos")
    public List<Coche> getCoches() {
        return coches;
    }

    public void setCoches(List<Coche> coches) {
        this.coches = coches;
    }

    @ManyToOne
    @JoinColumn(name = "id_taller", referencedColumnName = "id", nullable = false)
    public Taller getTaller() {
        return taller;
    }

    public void setTaller(Taller taller) {
        this.taller = taller;
    }

    public Mecanico() {
    }

    public Mecanico(String nombremecanico, String apellido, Date fechaNacimiento, int aniosexperiencia) {
        this.nombremecanico = nombremecanico;
        this.apellido = apellido;
        this.fechaNacimiento = fechaNacimiento;
        this.aniosexperiencia = aniosexperiencia;
    }

    @Override
    public String toString() {
        return nombremecanico + " "  + apellido ;
    }

}
