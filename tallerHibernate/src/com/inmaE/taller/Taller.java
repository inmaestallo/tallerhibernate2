package com.inmaE.taller;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Taller {
    private int id;
    private String nombretaller;
    private Date fechaCreacion;
    private String telefono;
    private Integer numeroFactura;
    private List<Mecanico> mecanicos;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombretaller")
    public String getNombretaller() {
        return nombretaller;
    }

    public void setNombretaller(String nombretaller) {
        this.nombretaller = nombretaller;
    }

    @Basic
    @Column(name = "fecha_creacion")
    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Basic
    @Column(name = "numero_factura")
    public Integer getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(Integer numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Taller taller = (Taller) o;
        return id == taller.id &&
                Objects.equals(nombretaller, taller.nombretaller) &&
                Objects.equals(fechaCreacion, taller.fechaCreacion) &&
                Objects.equals(telefono, taller.telefono) &&
                Objects.equals(numeroFactura, taller.numeroFactura);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombretaller, fechaCreacion, telefono, numeroFactura);
    }

    @OneToMany(mappedBy = "taller")
    public List<Mecanico> getMecanicos() {
        return mecanicos;
    }

    public void setMecanicos(List<Mecanico> mecanicos) {
        this.mecanicos = mecanicos;
    }

    public Taller(String nombretaller, Date fechaCreacion, String telefono, int numeroFactura) {
        this.nombretaller = nombretaller;
        this.fechaCreacion = fechaCreacion;
        this.telefono = telefono;
        this.numeroFactura = numeroFactura;
    }

    public Taller() {
    }

    @Override
    public String toString() {
        return  nombretaller + " " + fechaCreacion ;
    }
}
