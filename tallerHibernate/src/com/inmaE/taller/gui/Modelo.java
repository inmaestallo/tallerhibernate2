package com.inmaE.taller.gui;

import com.inmaE.taller.Cliente;
import com.inmaE.taller.Coche;
import com.inmaE.taller.Mecanico;
import com.inmaE.taller.Taller;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.ArrayList;

public class Modelo {

    SessionFactory sessionFactory;

    public void desconectar() {
        //Cierro la factoria de sessiones
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
    }

    public void conectar() {
        Configuration configuracion = new Configuration();
        //Cargo el fichero Hibernate.cfg.xml
        configuracion.configure("hibernate.cfg.xml");

        //Indico la clase mapeada con anotaciones
        configuracion.addAnnotatedClass(Cliente.class);
        configuracion.addAnnotatedClass(Coche.class);
        configuracion.addAnnotatedClass(Mecanico.class);
        configuracion.addAnnotatedClass(Taller.class);

        //Creamos un objeto ServiceRegistry a partir de los parámetros de configuración
        //Esta clase se usa para gestionar y proveer de acceso a servicios
        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();

        //finalmente creamos un objeto sessionfactory a partir de la configuracion y del registro de servicios
        sessionFactory = configuracion.buildSessionFactory(ssr);

    }

    public void altaCoche(Coche nuevoCoche) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevoCoche);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public ArrayList<Coche> getCoches() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Coche");
        ArrayList<Coche> lista = (ArrayList<Coche>)query.getResultList();
        sesion.close();
        return lista;
    }

    public void modificar(Coche cocheSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(cocheSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void borrar(Coche cocheBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(cocheBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public ArrayList<Coche> getCochesCliente(Cliente clienteSeleccionado) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Coche WHERE Cliente = :prop");
        query.setParameter("prop", clienteSeleccionado);
        ArrayList<Coche> lista = (ArrayList<Coche>) query.getResultList();
        sesion.close();
        return lista;
    }
    public ArrayList<Cliente> getClienteCoche() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Cliente ");
        ArrayList<Cliente> lista = (ArrayList<Cliente>) query.getResultList();
        sesion.close();
        return lista;
    }
    public void altaCliente(Cliente nuevoCliente) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoCliente);
        sesion.getTransaction().commit();

        sesion.close();
    }
    public void modificarCliente(Cliente clienteSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(clienteSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }
    public void borrarCliente(Cliente clienteBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(clienteBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }
    public ArrayList<Cliente> getCliente() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Cliente");
        ArrayList<Cliente> listaCliente = (ArrayList<Cliente>)query.getResultList();
        sesion.close();
        return listaCliente;
    }
    public void altaMecanico(Mecanico nuevoMecanico) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevoMecanico);
        sesion.getTransaction().commit();

        sesion.close();
    }
    public void modificarMecanico(Mecanico mecanicoSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(mecanicoSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }
    public void borrarMecanico(Mecanico mecanicoBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(mecanicoBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }
    public ArrayList<Mecanico>getMecanico() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Mecanico");
        ArrayList<Mecanico> listaMecanico = (ArrayList<Mecanico>)query.getResultList();
        sesion.close();
        return listaMecanico;
    }
    public void altaTaller(Taller nuevoTaller) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevoTaller);
        sesion.getTransaction().commit();

        sesion.close();
    }
    public void modificarTaller(Taller tallerSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(tallerSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }
    public void borrarTaller(Taller tallerBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(tallerBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }
    public ArrayList<Taller> getTaller() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Taller");
        ArrayList<Taller> listaTaller = (ArrayList<Taller>)query.getResultList();
        sesion.close();
        return listaTaller;
    }

}
