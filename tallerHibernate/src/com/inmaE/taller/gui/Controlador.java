package com.inmaE.taller.gui;

import com.inmaE.taller.Cliente;
import com.inmaE.taller.Coche;
import com.inmaE.taller.Mecanico;
import com.inmaE.taller.Taller;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class Controlador implements ActionListener, ListSelectionListener {
    private Vistaa vista;
    private Modelo modelo;
    
    public Controlador(Vistaa vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        addActionListeners(this);
        addListSelectionListener(this);
    }

    private void addActionListeners(ActionListener listener) {
        vista.conexionItem.addActionListener(listener);
        vista.salirItem.addActionListener(listener);
        vista.altaClienteButton.addActionListener(listener);
        vista.modificarClienteButton.addActionListener(listener);
        vista.listaDeClientesButton.addActionListener(listener);
        vista.listarClienteButton.addActionListener(listener);
        vista.borrarClienteButton.addActionListener(listener);
        vista.altaCocheButton.addActionListener(listener);
        vista.modificarCocheButton.addActionListener(listener);
        vista.listarCocheButton.addActionListener(listener);
        vista.borrarCocheButton.addActionListener(listener);
        vista.altaTallerButton.addActionListener(listener);
        vista.modificarTallerButton.addActionListener(listener);
        vista.listarTallerButton.addActionListener(listener);
        vista.borrarTallerButton.addActionListener(listener);
        vista.altaMecanicoButton.addActionListener(listener);
        vista.modificarMecanicoButton.addActionListener(listener);
        vista.listaMecanicoButton.addActionListener(listener);
        vista.borrarMecanicoButton.addActionListener(listener);
    }

    private void addListSelectionListener(ListSelectionListener listener) {

        vista.listCoche.addListSelectionListener(listener);
        vista.listCliente.addListSelectionListener(listener);
        vista.listMecanico.addListSelectionListener(listener);
        vista.listTaller.addListSelectionListener(listener);
        vista.listClientesCoche.addListSelectionListener(listener);
        vista.listCocheCliente.addListSelectionListener(listener);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String comando = e.getActionCommand();

        switch(comando){
            case "Salir":
                modelo.desconectar();
                System.exit(0);
                break;
            case "Conectar":
                vista.conexionItem.setEnabled(false);
                modelo.conectar();
                break;

            case "Alta Coche":
                Coche nuevoCoche = new  Coche();
                nuevoCoche.setModelo(vista.txtModeloCoche.getText());
                nuevoCoche.setAnioscoche(Integer.parseInt(vista.txtAniosCocheCoche.getText()));
                nuevoCoche.setFechaCreacion(Date.valueOf(String.valueOf(vista.fechaCreacionCoche)));
                modelo.altaCoche(nuevoCoche);

                break;

            case "Listar Coche":
                listarCoches(modelo.getCoches());

                break;


            case "Modificar Coche":
                Coche cocheSeleccion = (Coche)vista.listCoche.getSelectedValue();
                cocheSeleccion.setModelo(vista.txtModeloCoche.getText());
                cocheSeleccion.setAnioscoche((Integer.parseInt(vista.txtAniosCocheCoche.getText())));
                cocheSeleccion.setFechaCreacion(Date.valueOf(String.valueOf(vista.fechaCreacionCoche)));
                cocheSeleccion.setCliente((Cliente) vista.listClientesCoche.getSelectedValue());
                System.out.println((Coche)vista.listCoche.getSelectedValue());
                modelo.modificar(cocheSeleccion);

                break;

            case "Borrar Coche":
                Coche cocheBorrado  = (Coche)vista.listCoche.getSelectedValue();
                modelo.borrar(cocheBorrado);
                break;

            case "Lista de clientes":
                listarClienteCoche(modelo.getClienteCoche());
                break;


            case "Alta cliente":
                Cliente nuevoCliente = new Cliente();
                nuevoCliente.setNombre(vista.txtNombreCliente.getText());
                nuevoCliente.setFechaNacimiento(Date.valueOf(String.valueOf(vista.fechaNacimientoCliente)));
                nuevoCliente.setAnioscoche(Integer.parseInt(vista.txtAniosCocheCliente.getText()));
                modelo.altaCliente(nuevoCliente);
                break;

            case "Listar cliente":
                listarClientes(modelo.getCliente());
                break;


            case "Modificar cliente":
                Cliente clienteSeleccion = (Cliente) vista.listCliente.getSelectedValue();
                clienteSeleccion.setNombre(vista.txtNombreCliente.getText());
                clienteSeleccion.setFechaNacimiento(Date.valueOf(String.valueOf(vista.fechaNacimientoCliente)));
                clienteSeleccion.setAnioscoche(Integer.parseInt(vista.txtAniosCocheCliente.getText()));
                modelo.modificarCliente(clienteSeleccion);

                break;
            case "Borrar cliente":
                Cliente clienteBorrado = (Cliente) vista.listCliente.getSelectedValue();
                modelo.borrarCliente(clienteBorrado);
                break;
            case "Alta Mecanico":
                Mecanico nuevoMecanico = new Mecanico();
                nuevoMecanico.setNombremecanico(vista.txtNombreMecanico.getText());
                nuevoMecanico.setApellido(vista.txtApellidoMecanico.getText());
                nuevoMecanico.setFechaNacimiento(Date.valueOf(String.valueOf(vista.fechaNacimientMecanico)));
                nuevoMecanico.setAniosexperiencia(Integer.parseInt(vista.txtAniosExperienciaMecanico.getText()));


                modelo.altaMecanico(nuevoMecanico);
                break;

            case "Lista Mecanico":
                listarMecanico(modelo.getMecanico());
                break;
            case "Modificar Mecanico":
                Mecanico mecanicoSeleccion = (Mecanico) vista.listMecanico.getSelectedValue();
                mecanicoSeleccion.setNombremecanico(vista.txtNombreMecanico.getText());
                mecanicoSeleccion.setApellido(vista.txtApellidoMecanico.getText());
                mecanicoSeleccion.setFechaNacimiento(Date.valueOf(String.valueOf(vista.fechaNacimientMecanico)));
                mecanicoSeleccion.setAniosexperiencia(Integer.parseInt(vista.txtAniosExperienciaMecanico.getText()));
                modelo.modificarMecanico(mecanicoSeleccion);
                break;
            case "Borrar Mecanico":
                Mecanico mecanicoBorrado = (Mecanico) vista.listMecanico.getSelectedValue();
                modelo.borrarMecanico(mecanicoBorrado);
                break;

            case "Alta Taller":
                Taller nuevoTaller = new Taller();
                nuevoTaller.setNombretaller(vista.txtNombreTaller.getText());
                nuevoTaller.setFechaCreacion(Date.valueOf(String.valueOf(vista.fechaTaller)));
                nuevoTaller.setTelefono(vista.txtTelefonoTaller.getText());
                nuevoTaller.setNumeroFactura(Integer.valueOf(vista.txtNumeroFactura.getText()));
                modelo.altaTaller(nuevoTaller);

                break;
            case "Listar Taller":
                listarTaller(modelo.getTaller());
                break;

            case "Modificar Taller":
                Taller tallerSeleccion = (Taller) vista.listTaller.getSelectedValue();
                tallerSeleccion.setNombretaller(vista.txtNombreTaller.getText());
                tallerSeleccion.setFechaCreacion(Date.valueOf(String.valueOf(vista.fechaTaller)));
                tallerSeleccion.setTelefono(vista.txtTelefonoTaller.getText());
                tallerSeleccion.setNumeroFactura(Integer.valueOf(vista.txtNumeroFactura.getText()));

                modelo.modificarTaller(tallerSeleccion);
                break;
            case "Borrar Taller":
                Taller borrartaller = (Taller) vista.listTaller.getSelectedValue();
                modelo.borrarTaller(borrartaller);
                break;
        }




    }
    private void listarClientes(ArrayList<Cliente> clientes) {
        vista.dlmClientes.clear();
        for(Cliente cliente : clientes){
            vista.dlmClientes.addElement(cliente);
        }
    }

    private void listarMecanico(ArrayList<Mecanico> mecanicos) {
        vista.dlmMecanicos.clear();
        for(Mecanico mecanico: mecanicos){
            vista.dlmMecanicos.addElement(mecanico);
        }
    }
    private void listarTaller(ArrayList<Taller> tallers) {
        vista.dlmTalleres.clear();
        for(Taller taller: tallers){
            vista.dlmTalleres.addElement(taller);
        }
    }

    public void listarCoches(ArrayList<Coche> lista){
        vista.dlmCoches.clear();
        for(Coche uncoche : lista){
            vista.dlmCoches.addElement(uncoche);
        }
    }

    public void listarClienteCoche(ArrayList<Cliente>lista){
        vista.dlmClienteCoches.clear();
        for(Cliente uncliente : lista){
            vista.dlmClienteCoches.addElement(uncliente);
        }
    }

    public void listarCochesCliente(List<Coche> lista){
        vista.dlmCocheClientes.clear();
        for(Coche uncoche : lista){
            vista.dlmCocheClientes.addElement(uncoche);
        }
    }
    @Override
    public void valueChanged(ListSelectionEvent e) {

        if(e.getValueIsAdjusting()) {
            if (e.getSource() == vista.listCoche) {
                Coche cocheSeleccion = (Coche) vista.listCoche.getSelectedValue();
                vista.txtIdCoche.setText(String.valueOf(cocheSeleccion.getId()));
                vista.txtModeloCoche.setText(cocheSeleccion.getModelo());
                vista.txtAniosCocheCoche.setText(String.valueOf(cocheSeleccion.getAnioscoche()));
                vista.fechaCreacionCoche.setDate(cocheSeleccion.getFechaCreacion().toLocalDate());


            if (cocheSeleccion.getCliente() != null) {
                    vista.txtCliente.setText(cocheSeleccion.getCliente().toString());
                } else {
                    vista.txtCliente.setText("");
                }


            }
            //


            if(e.getSource() == vista.listCliente) {
                Cliente clienteSeleccion = (Cliente) vista.listCliente.getSelectedValue();
                vista.txtIdCliente.setText(String.valueOf(clienteSeleccion.getId()));
                vista.txtNombreCliente.setText(clienteSeleccion.getNombre());
                vista.fechaNacimientoCliente.setDate(clienteSeleccion.getFechaNacimiento().toLocalDate());
                vista.txtAniosCocheCliente.setText(String.valueOf(clienteSeleccion.getAnioscoche()));


        }
            if(e.getSource() == vista.listMecanico) {
                Mecanico mecanicoSeleccion = (Mecanico) vista.listMecanico.getSelectedValue();
                vista.txtIdMecanico.setText(String.valueOf(mecanicoSeleccion.getId()));
                vista.txtNombreMecanico.setText(mecanicoSeleccion.getNombremecanico());
                vista.txtApellidoMecanico.setText(mecanicoSeleccion.getApellido());
                vista.fechaNacimientMecanico.setDate(mecanicoSeleccion.getFechaNacimiento().toLocalDate());
                vista.txtAniosExperienciaMecanico.setText(String.valueOf(mecanicoSeleccion.getAniosexperiencia()));
                vista.idTallerMecanico.setText(String.valueOf(mecanicoSeleccion.getTaller()));

            }
            if(e.getSource() == vista.listTaller) {
                Taller tallerSeleccion = (Taller) vista.listTaller.getSelectedValue();
                vista.txtIdTaller.setText(String.valueOf(tallerSeleccion.getId()));
                vista.txtNombreTaller.setText(tallerSeleccion.getNombretaller());
                vista.fechaTaller.setDate(tallerSeleccion.getFechaCreacion().toLocalDate());
                vista.txtTelefonoTaller.setText(tallerSeleccion.getTelefono());
                vista.txtNumeroFactura.setText(String.valueOf(tallerSeleccion.getNumeroFactura()));



            }
        }else{
                if(e.getSource() == vista.listClientesCoche){
                   Cliente clienteSeleccionado = (Cliente) vista.listClientesCoche.getSelectedValue();
                   listarCochesCliente(modelo.getCochesCliente(clienteSeleccionado));

                }
            }
        }


}
