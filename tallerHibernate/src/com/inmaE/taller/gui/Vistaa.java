package com.inmaE.taller.gui;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

public class Vistaa {

    JFrame frame;
    JMenuItem conexionItem;
    JMenuItem salirItem;
     JTabbedPane tabbedPane1;
     JPanel panel1;


     JTextField txtNombreMecanico;
     JTextField txtApellidoMecanico;
     JTextField txtAniosExperienciaMecanico;
     JTextField txtNombreTaller;
     JTextField txtTelefonoTaller;
     JTextField txtNumeroFactura;
     JTextField txtModeloCoche;
     JTextField txtAniosCocheCoche;
     JTextField txtCliente;
     JTextField txtNombreCliente;
     JTextField txtAniosCocheCliente;
     JTextField txtIdCliente;
     JTextField txtIdCoche;
     JTextField txtIdMecanico;
     JTextField txtIdTaller;



     JButton altaClienteButton;
     JButton listarClienteButton;
     JButton modificarClienteButton;
     JButton borrarClienteButton;
     JButton altaCocheButton;
     JButton listarCocheButton;
     JButton modificarCocheButton;
     JButton borrarCocheButton;
     JButton listaDeClientesButton;
     JButton altaMecanicoButton;
     JButton listaMecanicoButton;
     JButton modificarMecanicoButton;
     JButton borrarMecanicoButton;
     JButton altaTallerButton;
     JButton listarTallerButton;
     JButton modificarTallerButton;
     JButton borrarTallerButton;

    DefaultListModel dlmClientes;
    DefaultListModel dlmCoches;
    DefaultListModel dlmCocheClientes;
    DefaultListModel dlmClienteCoches;
    DefaultListModel dlmTalleres;
    DefaultListModel dlmMecanicos;


    JList listCliente;
    JList listCoche;
    JList listCocheCliente;
    JList listClientesCoche;
    JList listTaller;
    JList listMecanico;
    DatePicker fechaCreacionCoche;
    DatePicker fechaNacimientoCliente;
    DatePicker fechaNacimientMecanico;
    DatePicker fechaTaller;
    JTextField idTallerMecanico;


    public Vistaa() {

        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        crearMenu();
        crearModelos();
        frame.pack();
        frame.setVisible(true);



        frame.setLocationRelativeTo(null);
    }

    private void crearModelos() {


        dlmClientes= new DefaultListModel();
        listCliente.setModel(dlmClientes);
        dlmCoches= new DefaultListModel();
        listCoche.setModel(dlmCoches);
        dlmCocheClientes=new DefaultListModel();
        listCocheCliente.setModel(dlmCocheClientes);
        dlmClienteCoches=new DefaultListModel();
        listClientesCoche.setModel(dlmClienteCoches);
        dlmTalleres=new DefaultListModel();
        listTaller.setModel(dlmTalleres);
        dlmMecanicos=new DefaultListModel();
        listMecanico.setModel(dlmMecanicos);

    }

    private void crearMenu() {

        JMenuBar barra= new JMenuBar();
        JMenu menu= new JMenu("Archivo");
        conexionItem=new JMenuItem("Conectar");
        conexionItem.setActionCommand("Conectar");

        salirItem=new JMenuItem("Salir");
        salirItem.setActionCommand("Salir");

        menu.add(conexionItem);
        menu.add(salirItem);
        barra.add(menu);
        frame.setJMenuBar(barra);
    }
}
