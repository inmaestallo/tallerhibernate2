package com.inmaE.taller;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Entity
public class Coche {
    private int id;
    private String modelo;
    private int anioscoche;
    private Date fechaCreacion;
    private Cliente cliente;
    private List<Mecanico> mecanicos;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "modelo")
    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    @Basic
    @Column(name = "anioscoche")
    public int getAnioscoche() {
        return anioscoche;
    }

    public void setAnioscoche(int anioscoche) {
        this.anioscoche = anioscoche;
    }

    @Basic
    @Column(name = "fecha_creacion")
    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coche coche = (Coche) o;
        return id == coche.id &&
                anioscoche == coche.anioscoche &&
                Objects.equals(modelo, coche.modelo) &&
                Objects.equals(fechaCreacion, coche.fechaCreacion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, modelo, anioscoche, fechaCreacion);
    }

    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id", nullable = false)
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @ManyToMany
    @JoinTable(name = "mecanico_coche", catalog = "", schema = "tallermecanico", joinColumns = @JoinColumn(name = "id_mecanico", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_coche", referencedColumnName = "id", nullable = false))
    public List<Mecanico> getMecanicos() {
        return mecanicos;
    }

    public void setMecanicos(List<Mecanico> mecanicos) {
        this.mecanicos = mecanicos;
    }

    public Coche() {
    }

    public Coche(int id, String modelo, int anioscoche, Date fechaCreacion, Cliente cliente) {
        this.id = id;
        this.modelo = modelo;
        this.anioscoche = anioscoche;
        this.fechaCreacion = fechaCreacion;
        this.cliente = cliente;
    }

    @Override
    public String toString() {
        return  modelo + " " + anioscoche ;
    }
}
