package com.inmaE.taller;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Cliente {
    private int id;
    private String nombre;
    private Date fechaNacimiento;
    private int anioscoche;
    private List<Coche> coches;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "fecha_nacimiento")
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Basic
    @Column(name = "anioscoche")
    public int getAnioscoche() {
        return anioscoche;
    }

    public void setAnioscoche(int anioscoche) {
        this.anioscoche = anioscoche;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return id == cliente.id &&
                anioscoche == cliente.anioscoche &&
                Objects.equals(nombre, cliente.nombre) &&
                Objects.equals(fechaNacimiento, cliente.fechaNacimiento);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, fechaNacimiento, anioscoche);
    }

    @OneToMany(mappedBy = "cliente")
    public List<Coche> getCoches() {
        return coches;
    }

    public void setCoches(List<Coche> coches) {
        this.coches = coches;
    }

    public Cliente( String nombre, Date fechaNacimiento, int anioscoche) {

        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
        this.anioscoche = anioscoche;

    }

    public Cliente() {
    }
    @Override
    public String toString() {
        return  nombre + " " +fechaNacimiento ;
    }
}
