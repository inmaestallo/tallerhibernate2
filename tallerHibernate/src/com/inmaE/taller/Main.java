package com.inmaE.taller;


import com.inmaE.taller.gui.Controlador;
import com.inmaE.taller.gui.Modelo;
import com.inmaE.taller.gui.Vistaa;

import java.sql.Connection;

public class Main {

    public static void main(String[] args) {
        Vistaa vista =new Vistaa();
        Modelo modelo=new Modelo();
        Controlador controlador=new Controlador(vista,modelo);

    }

}